## Wordpress plugin that enabling autofill of Gravity Forms fields with server variables -- 

In this case UW-Madison NetID info, NetID and PVI values (the "P" in PVI is "public). **This is a simple and WIP demo.** Server variable might or might not have these exact names. 

For Gravity Forms docs on the subject, see [https://docs.gravityforms.com/using-dynamic-population/](https://docs.gravityforms.com/using-dynamic-population/)

Jump ahead? Go to edit tab of Gravity Forms, then "advanced tab," check "allow field to be populated dynamically," enter parameter "netid" or "pvi."

This could also be easily done as few lines a code added to the WP theme functions.php file. 