<?php

 class UwFormFiller {
   /*  
      WIP -  Follows direction at https://docs.gravityforms.com/category/developers/
       to add fillin of Gravity Forms fields with server variable 
       - in this case NetID and PVI (as they were available and safe)
   */

    public $pluginObject;

public function __construct( ) {
    // init language, constants, scripts, etc.
    add_action( 'init', array( $this, 'plugin_textdomain' ) );

    // add netid server varialbe to Gravity Forms 
    add_filter( 'gform_field_value_netid', array( $this,'uw_form_filler_populate_gf_netid' ));
    // add netid server varialbe to Gravity Forms 
    add_filter( 'gform_field_value_pvi', array( $this,'uw_form_filler_populate_gf_pvi' ));

    //shortcode  -not used
    //add_action( 'init', array( $this, 'uw_form_filler_register_plugin_shortcodes' ) );
}
public function plugin_textdomain() {
  $domain = 'uw_form_filler'; 
  $locale = apply_filters( 'plugin_locale', get_locale(), $domain );
  load_textdomain( $domain, WP_LANG_DIR.'/'.$domain.'/'.$domain.'-'.$locale.'.mo' );
  load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
}

public function uw_form_filler_display_netid($atts) {
  $netid =   $_SERVER['HTTP_REMOTE_USER']; //$_SERVER['SERVER_NAME'];
  echo '<div id="uw-form-filler">' . $netid . '</div>';
}

function uw_form_filler_populate_gf_netid( $value ) {

  if (isset($_SERVER['REMOTE_USER']) ) {
    $netid =  $_SERVER['REMOTE_USER'];
    if($netid === '') {
      $netid =  'unknown - please login to NetID';
    }
  }
  else {
    $netid =  'unknown - please login to NetID';}
  return $netid;
}

function uw_form_filler_populate_gf_pvi( $value ) {

  if (isset($_SERVER['wiscEduPVI'])) {
  $netid =  $_SERVER['wiscEduPVI'];}
  else {
    $netid =  '-';}
  return $netid;
}

public function uw_form_filler_register_plugin_shortcodes() {
  // [uw-form-filler-netid]
  add_shortcode('uw-form-filler-netid', array( $this, 'uw_form_filler_display_netid' ));
}
 }