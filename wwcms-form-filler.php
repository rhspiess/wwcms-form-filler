<?php
/*
Plugin Name: UW Form Filler -- wip
Plugin URI: NA
Description:  plugin for Wordpress blogging tool
Version: 1.0
Author: DoIT Communications/DoIT ADI
Author URI: https://git.doit.wisc.edu/DoITComm
Author Email: webchanges@doit.wisc.edu
License:  Board of Regents of the University of Wisconsin System
 *  Text Domain: plugin-name
 * Domain Path: /languages
 */

if ( ! defined( 'WPINC' ) ) { die; }  // If this file is called directly, abort.

// activate/deactivate paths and hooks

// the plugin
require_once plugin_dir_path( __FILE__ ) . 'wwcms-form-filler-plugin.php';

if( ! function_exists('run_UwFormFiller') ) {

function run_UwFormFiller() {

    $plugin = new UwFormFiller();
    //$plugin->run();
  }
}
run_UwFormFiller();